package be.vub.alexandre;

import be.vub.alexandre.data.models.Comment;
import be.vub.alexandre.data.readers.RedditCommentLoader;

import javax.crypto.*;
import java.io.IOException;
import java.security.*;
import java.util.ArrayList;

public class BenchMarkServer extends TaskServer {

    private ComputingProtocol client;

    private int benchSize = 200;


    private ArrayList<Comment> benchList;
    private String file = "dataset_1.json";
    private ArrayList<Comment> comments;

    private float correctResult = (float) 22.188702 ;

    {
        try {
            comments = RedditCommentLoader.readData(file);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    private String plaintext = "Distributed Reddit Analyser";


    //private Hashtable<ComputingProtocol, byte[]> keyTable = new Hashtable<ComputingProtocol, byte[]>();


    public void BenchmarkServer() throws NoSuchAlgorithmException {
        TaskServer();
        benchList = new ArrayList<Comment>(comments.subList(0, benchSize));


    }

    public void nextClient(ComputingProtocol client){
        this.client = client;
    }

    @Override
    public void start(ComputingProtocol taskServer)  {
        ComputingProtocol benchserver = taskServer;
        // Special start for benchmarking
        try {

            byte[] symmetricKey = generateAESKey();

            // Ask the publickey of the client
            PublicKey pubkey = client.getPublicKey();

            // Encrypt our symmetric key with the publickey
            String encryptedKey = encryptKey(pubkey, symmetricKey);

            // Send it to the client
            client.sendRegisteredKey(encryptedKey);
            keyTable.put(client, symmetricKey);
            System.out.print(keyTable);
            // Encrypt the list
            byte[] clientBytes = encryptData(benchList, symmetricKey);

            // Send it to the Client
            PrivateKey privateKey = security.getPrivateKey();
            PublicKey publicKey = security.getPublicKey();
            byte[] signature = security.sign(plaintext, privateKey);
            if (client.store(clientBytes, publicKey, signature, benchserver, benchSize)) {
                client.start(benchserver);
            }
        }
        catch (NoSuchAlgorithmException e){
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
        } catch (BadPaddingException e) {
            e.printStackTrace();
        } catch (IllegalBlockSizeException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    @Override
    public void getResult(byte[] result, ComputingProtocol client, PublicKey publicKey, byte[] signature)   {
        try {
            security.verify(plaintext,signature,publicKey);
            float decryptedResult = decryptScore(result, client);
            System.out.println(decryptedResult);
            System.out.print(correctResult);
            boolean safe = (decryptedResult == correctResult );
            System.out.println(safe);
            registryserver.isSafe(client, safe);

        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (BadPaddingException e) {
            e.printStackTrace();
        } catch (IllegalBlockSizeException e) {
            e.printStackTrace();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}
