package be.vub.alexandre.data.solutions;


import be.vub.alexandre.data.models.Comment;
import be.vub.alexandre.data.readers.RedditCommentLoader;
import com.vader.sentiment.analyzer.SentimentAnalyzer;
import com.vader.sentiment.util.ScoreType;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ForkJoinPool;

public class ParallelFilterAnalyser {

    public static float analyse(List<Comment> comments, int p) {
        float totalCompoundScore = 0;
        SentimentAnalyzer sentimentAnalyzer = new SentimentAnalyzer();

        try {
            // Multiple files can be provided. These will be read one after the other.


            final String brand = "BMW";

            // If you want to sequentially fitler the data based on the comment (to reduce a data set), use a lambda
            // List<Comment> comments = RedditCommentLoader.readData(data, comment -> comment.body.contains("BMW"));

            // System.out.println(comments.size());

            ForkJoinPool pool = new ForkJoinPool(p);

            int counter = 0;

            for (Comment comment : comments) {

                if (pool.invoke(new BrandAnalyserFilterTask(comment.body, brand))) {

                    counter++;
                    sentimentAnalyzer.setInputString(comment.body);
                    sentimentAnalyzer.setInputStringProperties();
                    sentimentAnalyzer.analyze();

                    Map<String, Float> inputStringPolarity = sentimentAnalyzer.getPolarity();
                    float commentCompoundScore = inputStringPolarity.get(ScoreType.COMPOUND);

                    totalCompoundScore += commentCompoundScore;
                }
            }

            System.out.println("average compound score: " + totalCompoundScore / counter);
        }
        catch (IOException e) {
            System.out.println(e.toString());
        }
        return totalCompoundScore;
    }
}
