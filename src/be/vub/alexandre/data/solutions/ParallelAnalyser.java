
package be.vub.alexandre.data.solutions;


import be.vub.alexandre.data.models.Comment;
import be.vub.alexandre.data.readers.RedditCommentLoader;
import com.vader.sentiment.analyzer.SentimentAnalyzer;
import com.vader.sentiment.util.ScoreType;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ForkJoinPool;

public class ParallelAnalyser {

    public static float analyse(List<Comment> comments, int p) {
        float totalCompoundScore = 0;

            totalCompoundScore = executeAnalyzer(comments, p);

            System.out.println("average compount score:" + totalCompoundScore / comments.size());


        return totalCompoundScore;
    }

    public static float executeAnalyzer(List<Comment> comments, int p) {
        ForkJoinPool pool = new ForkJoinPool(p);
        float res = pool.invoke(new AnalyserTask(comments));
        return res;
    }

}