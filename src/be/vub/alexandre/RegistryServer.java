package be.vub.alexandre;

import java.rmi.server.UnicastRemoteObject;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.util.*;

import java.rmi.*;
import java.rmi.registry.*;

public class RegistryServer implements ComputingProtocol {

    private PriorityQueue<ClientInfo> sleepingClients = new PriorityQueue<ClientInfo>();
    private PriorityQueue<ClientInfo> activeClients = new PriorityQueue<ClientInfo>();
    private Hashtable<Integer, ClientInfo> allClients = new Hashtable<>();
    private int id = 0;

    private BenchMarkServer benchMarkServer;

    private int getId(){
        id++;
        return id;
    }

    public void RegistryServer(){
        goOnline();
    }

    public void goOnline() {
        try {
            LocateRegistry.createRegistry(1099);
            UnicastRemoteObject.exportObject(this, 0);
            LocateRegistry.getRegistry().rebind("RegistryServer", this);
            benchMarkServer = new BenchMarkServer();
            benchMarkServer.BenchmarkServer();
        } catch(RemoteException e){
            System.out.println("Server remote exception " + e.getMessage());
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

    }


    @Override
    public int getsize() throws RemoteException {
        return 0;
    }

    @Override
    public boolean store(byte[] data, PublicKey publicKey, byte[] signature, ComputingProtocol server, int listSize) throws RemoteException {
        return false;
    }

    @Override
    public void start(ComputingProtocol taskServer) throws RemoteException {
    }

    @Override
    public int add(ComputingProtocol clientWorker) throws RemoteException {
        int clientID = getId();
        benchMarkServer.nextClient(clientWorker);
        benchMarkServer.start(benchMarkServer);


//        addToQueue(clientWorker);
        //sleepingClients.add(clientWorker);
        return clientID;
    }

    private void addToQueue(ClientInfo clientInfo) throws RemoteException {
        //sleepingClients.add(clientInfo);
        int clientID = clientInfo.getClient().getID();
        allClients.put(clientID,clientInfo);
    }

    @Override
    public boolean remove(ClientWorker clientWorker) throws RemoteException {
        int clientID = clientWorker.getID();
        sleepingClients.remove(clientID);
        return true;
    }

    @Override
    public ComputingProtocol get() throws RemoteException {
        // Always take one of the sleeping clients
        ClientInfo clientInfo = sleepingClients.peek();
        if (clientInfo != null){
            return clientInfo.getClient();
        }
        // Then take the best of the active ones
        if (activeClients.isEmpty()){
            return null;
        }
        clientInfo = activeClients.peek();
        clientInfo.addJob();
        return clientInfo.getClient();
    }

    @Override
    public boolean set(ComputingProtocol clientWorker , boolean status, String fileAddress) throws RemoteException {
        int clientID = clientWorker.getID();
        if (status){
            ClientInfo client =sleepingClients.poll();
            if (client != null) {
                activeClients.add(client);
            }
        }
        else {
            ClientInfo clientInfo = allClients.get(clientID);
            if (clientInfo != null) {
                activeClients.remove(clientInfo);
                sleepingClients.add(clientInfo);
            }

        }
        return true;
    }

    @Override
    public void getResult(byte[] result, ComputingProtocol clientworker, PublicKey publicKey, byte[] signature) throws RemoteException {
    }

    @Override
    public int getID(){
        return 0;
    }

    @Override
    public PublicKey getPublicKey() throws RemoteException {
        return null;
    }

    @Override
    public void sendRegisteredKey(String encryptedkey) throws RemoteException {

    }

    @Override
    public int getDiskSize() throws RemoteException {
        return 0;
    }

    @Override
    public float getScore() throws RemoteException {
        return 0;
    }

    @Override
    public void isSafe(ComputingProtocol client, boolean safe) throws RemoteException {
        if (safe){
            float score =client.getScore();
            System.out.println(score);
            ClientInfo clientInfo = new ClientInfo(client, score);
            addToQueue(clientInfo);
        }
    }

    @Override
    public boolean isBusy() throws RemoteException {
        return false;
    }

    @Override
    public void removeJob(int id){
        ClientInfo client = allClients.get(id);
        client.removeJob();
    }


}
