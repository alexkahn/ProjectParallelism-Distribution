package be.vub.alexandre;

import java.rmi.Remote;
import java.rmi.RemoteException;

import java.security.PublicKey;

public interface ComputingProtocol extends Remote {

    public int getsize() throws RemoteException;

    public boolean store(byte[] data, PublicKey publicKey, byte[] signature, ComputingProtocol server, int listSize) throws RemoteException;

    public void start(ComputingProtocol taskServer) throws RemoteException;

    public int add(ComputingProtocol clientWorker) throws RemoteException;

    public boolean remove(ClientWorker clientWorker) throws RemoteException;

    public ComputingProtocol get() throws RemoteException;

    public boolean set(ComputingProtocol clientWorker , boolean status, String fileAddress) throws RemoteException;

    public void getResult(byte[] result, ComputingProtocol client, PublicKey publicKey, byte[] signature) throws RemoteException;

    public int getID() throws RemoteException;

    public PublicKey getPublicKey() throws RemoteException;

    public void sendRegisteredKey(String encryptedkey) throws RemoteException;

    public int getDiskSize() throws RemoteException;

    public float getScore() throws RemoteException;

    public void isSafe(ComputingProtocol client, boolean safe) throws RemoteException;

    public boolean isBusy() throws RemoteException;

    public void removeJob(int id) throws RemoteException;
}
