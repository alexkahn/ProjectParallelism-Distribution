package be.vub.alexandre;

import be.vub.alexandre.data.models.Comment;
import com.sun.org.apache.xml.internal.security.utils.Base64;

import javax.crypto.*;
import javax.crypto.spec.SecretKeySpec;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.nio.ByteBuffer;
import java.security.*;
import java.util.ArrayList;

import static org.apache.commons.lang.CharEncoding.UTF_8;

public class Security {

    private PublicKey publicKey;
    private PrivateKey privateKey;

    // Encryption

    //Generate Public key
    public void generateKeyPair() throws NoSuchAlgorithmException {
        KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance("RSA");
        keyPairGenerator.initialize(1024);
        KeyPair keyPair = keyPairGenerator.generateKeyPair();
        privateKey = keyPair.getPrivate();
        publicKey = keyPair.getPublic();
    }

//    private byte[] decryptAsymmetric(String encryptedkey, ComputingProtocol taskServer) throws InvalidKeyException, BadPaddingException, IllegalBlockSizeException, NoSuchPaddingException, NoSuchAlgorithmException {
//        Cipher dipher = Cipher.getInstance("RSA");
//        dipher.init(Cipher.DECRYPT_MODE, privateKey);
//        byte[] decryptedSymmetricKey;
//        decryptedSymmetricKey = dipher.doFinal(Base64.decodeBase64(encryptedkey));
//
//        ServerInfo serverInfo = new ServerInfo(taskServer, decryptedSymmetricKey);
//        serverInfos.add(serverInfo);
//
//        return decryptedSymmetricKey;
//    }
//
//    private ArrayList<Comment> decryptSymmetric(byte[] dataList, byte[] key) throws IOException, ClassNotFoundException, BadPaddingException, IllegalBlockSizeException, InvalidKeyException, NoSuchPaddingException, NoSuchAlgorithmException {
//
//        Cipher cipher = Cipher.getInstance("AES");
//        SecretKey secKey = new SecretKeySpec(key, "AES");
//
//        cipher.init(Cipher.DECRYPT_MODE, secKey);
//        byte[] textDecrypted = cipher.doFinal(dataList);
//        ByteArrayInputStream bis = new ByteArrayInputStream(textDecrypted);
//        ObjectInputStream ois = new ObjectInputStream(bis);
//        ArrayList<Comment> result = (ArrayList<Comment>) ois.readObject();
//
//        return result;
//
//    }
//
//    private byte[] encryptScore(float score, byte[] key) throws NoSuchPaddingException, NoSuchAlgorithmException, InvalidKeyException, IOException, BadPaddingException, IllegalBlockSizeException {
//        SecretKey secKey = new SecretKeySpec(key,"AES");
//
//        Cipher cipher = Cipher.getInstance("AES");
//
//        cipher.init(Cipher.ENCRYPT_MODE, secKey);
//
//        byte[] msg= ByteBuffer.allocate(4).putFloat(score).array();
//        return cipher.doFinal(msg);
//    }

    // Code for signing

    public static byte[] sign(String plainText, PrivateKey privateKey) throws Exception {
        Signature privateSignature = Signature.getInstance("SHA224withRSA");
        privateSignature.initSign(privateKey);
        privateSignature.update(plainText.getBytes(UTF_8));

        byte[] signature = privateSignature.sign();
        return signature;
    }

    public static boolean verify(String plainText, byte[] signature, PublicKey publicKey) throws Exception {
        Signature publicSignature = Signature.getInstance("SHA224withRSA");
        publicSignature.initVerify(publicKey);
        publicSignature.update(plainText.getBytes(UTF_8));

        return publicSignature.verify(signature);
    }

    public PublicKey getPublicKey() {
        return publicKey;
    }

    public PrivateKey getPrivateKey() {
        return privateKey;
    }
}
