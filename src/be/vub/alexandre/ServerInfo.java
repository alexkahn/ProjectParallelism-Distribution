package be.vub.alexandre;

public class ServerInfo {

    public ServerInfo(ComputingProtocol server, byte[] decryptedSymmetricKey){
        this.server = server;
        this.decryptedSymmetricKey = decryptedSymmetricKey;
    }

    private ComputingProtocol server;
    private byte[] decryptedSymmetricKey;


    public byte[] getDecryptedSymmetricKey() {
        return decryptedSymmetricKey;
    }

    public ComputingProtocol getServer() {

        return server;
    }
}
