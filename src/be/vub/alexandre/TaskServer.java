package be.vub.alexandre;

import be.vub.alexandre.data.models.Comment;
import java.io.*;
import be.vub.alexandre.data.readers.RedditCommentLoader;
import org.apache.commons.codec.binary.Base64;


import javax.crypto.*;
import javax.crypto.spec.SecretKeySpec;
import java.nio.ByteBuffer;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.security.*;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

import static org.apache.commons.lang.CharEncoding.UTF_8;

public class TaskServer implements ComputingProtocol {

    protected boolean worker = true;
    protected boolean sleeper = false;

    protected ComputingProtocol taskserver;
    protected ComputingProtocol registryserver;

    protected float compoundResult;
    protected int beginsize;

    protected String file;
    List<Comment> comments;

    //    protected PublicKey publicKey;
//    protected byte[] symmetricKey;
//    protected String encryptedkey;
    protected Security security;

    protected Hashtable<ComputingProtocol, byte[]> keyTable; // Save the client as key and then the AES key

    protected String plaintext = "Distributed Reddit Analyser";


    public void TaskServer() throws NoSuchAlgorithmException {
//        generateAESKey();
        security = new Security();
        security.generateKeyPair();
        keyTable = new Hashtable<ComputingProtocol, byte[]>();
        connect();
    }


    @Override
    public int getID() {
        return 0;
    }

    @Override
    public PublicKey getPublicKey() throws RemoteException {
        return security.getPublicKey();
    }


    protected void connect() {
        try {
            Registry reg = LocateRegistry.getRegistry();
            taskserver = (ComputingProtocol) UnicastRemoteObject.exportObject(this, 0);
            registryserver = (ComputingProtocol) reg.lookup("RegistryServer");
        } catch (RemoteException e) {
            System.out.println("Client remote exception: " + e.getMessage());
        } catch (NotBoundException e) {
            System.out.println("Client not bound exception: " + e.getMessage());
        }
    }

    public static byte[] encrypt(PublicKey publicKey, String message) throws Exception {
        Cipher cipher = Cipher.getInstance("RSA");
        cipher.init(Cipher.ENCRYPT_MODE, publicKey);

        return cipher.doFinal(message.getBytes());
    }

    @Override
    public int getsize() {
        return 0;
    }

    @Override
    public boolean store(byte[] data, PublicKey publicKey, byte[] signature, ComputingProtocol server, int listSize) {
        return false;
    }

    @Override
    public void start(ComputingProtocol taskServer) {
        try {
            List<Comment> remainingComments = comments;
            int size = remainingComments.size();
            int i = 0;
            while (i < size) {

                ComputingProtocol client = registryserver.get();
                if (client != null) {
                    registryserver.set(client, worker, null);
                    int maxSize = client.getsize();
                    if (client.isBusy()) {
                        maxSize = Math.min(maxSize, client.getDiskSize());
                        if (maxSize == 0) {
                            Thread.sleep(5000);
                            comments = new ArrayList<Comment>(comments.subList(i, size));
                            start(taskServer);
                            break;
                        }
                    }
                    int upperBound = Math.min(i + maxSize, size);
                    ArrayList<Comment> clientList = new ArrayList<Comment>(comments.subList(i, upperBound));
                    // Before we store the result we will
                    byte[] symmetricKey;
                    if (!keyTable.containsKey(client)){
                        System.out.println(keyTable.containsKey(client));
                        symmetricKey = generateAESKey();
                        keyTable.put(client, symmetricKey);
                    }
                    symmetricKey = keyTable.get(client);


                    // Ask the publickey of the client
                    PublicKey pubkey = client.getPublicKey();

                    // Encrypt our symmetric key with the publickey
                    String encryptedKey = encryptKey(pubkey, symmetricKey);

                    // Send it to the client
                    client.sendRegisteredKey(encryptedKey);

                    // Encrypt the list
                    byte[] clientBytes = encryptData(clientList, symmetricKey);

                    PrivateKey privateKey = security.getPrivateKey();
                    PublicKey publicKey = security.getPublicKey();

                    // Send it to the Client
                    byte[] signature = security.sign(plaintext, privateKey);
                    int listsize = upperBound - i;
                    if (client.store(clientBytes, publicKey, signature, taskserver, listsize)) {
                        client.start(taskserver);
                    }

                    System.out.println("Job done or saved?");
                    i = i + maxSize + 1;


                } else {
                    Thread.sleep(5000);
                    comments = new ArrayList<Comment>(comments.subList(i, size));
                    start(taskServer);
                }

            }
        } catch (RemoteException e) {
            System.out.println("No clients left");
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        } catch (IllegalBlockSizeException e) {
            e.printStackTrace();
        } catch (BadPaddingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int add(ComputingProtocol clientWorker) {
        return 0;
    }

    @Override
    public boolean remove(ClientWorker clientWorker) {
        return false;
    }

    @Override
    public ClientWorker get() {
        return null;
    }

    @Override
    public boolean set(ComputingProtocol clientWorker, boolean status, String fileAddress) {
        file = fileAddress;
        fileToList();
        return true;
    }

    protected void fileToList() {
        try {
            comments = RedditCommentLoader.readData(file);
            comments = comments.subList(0, 5000);
            beginsize = comments.size();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void getResult(byte[] result, ComputingProtocol client, PublicKey publicKey, byte[] signature) {
        try {
            security.verify(plaintext,signature,publicKey);
            float decryptedResult = decryptScore(result, client);
            compoundResult += decryptedResult;
            System.out.print(compoundResult / beginsize);
        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (BadPaddingException e) {
            e.printStackTrace();
        } catch (IllegalBlockSizeException e) {
            e.printStackTrace();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    // Encryption

    // Generate AES Key
    protected byte[] generateAESKey() throws NoSuchAlgorithmException {
        KeyGenerator generator = KeyGenerator.getInstance("AES");
        generator.init(128);
        SecretKey key = generator.generateKey();
        byte[] symmetricKey = key.getEncoded();
//        System.out.println("key : " + symmetricKey);
        return symmetricKey;
    }

    protected String encryptKey(PublicKey pubkey, byte[] symmetricKey) throws BadPaddingException, IllegalBlockSizeException, NoSuchPaddingException, NoSuchAlgorithmException, InvalidKeyException {
        Cipher cipher = Cipher.getInstance("RSA");
        cipher.init(Cipher.ENCRYPT_MODE, pubkey);
        String encryptedkey = Base64.encodeBase64String(cipher.doFinal(symmetricKey));
        return encryptedkey;
    }

    protected byte[] encryptData(ArrayList<Comment> data, byte[] key) throws InvalidKeyException, NoSuchPaddingException, NoSuchAlgorithmException, IOException, BadPaddingException, IllegalBlockSizeException {
        SecretKey secKey = new SecretKeySpec(key, "AES");

        Cipher cipher = Cipher.getInstance("AES");

        cipher.init(Cipher.ENCRYPT_MODE, secKey);

        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        ObjectOutputStream oos = new ObjectOutputStream(bos);
        oos.writeObject(data);
        byte[] text = bos.toByteArray();
        byte[] dataList = cipher.doFinal(text);
        return dataList;
    }

    protected float decryptScore(byte[] score, ComputingProtocol client) throws NoSuchPaddingException, NoSuchAlgorithmException, BadPaddingException, IllegalBlockSizeException, InvalidKeyException {
        byte[] symKey = keyTable.get(client);
        Cipher cipher = Cipher.getInstance("AES");
        SecretKey secKey = new SecretKeySpec(symKey, "AES");

        cipher.init(Cipher.DECRYPT_MODE, secKey);
        byte[] textDecrypted = cipher.doFinal(score);
        float res = ByteBuffer.wrap(textDecrypted).getFloat();
        return res;
    }

    public void sendRegisteredKey(String encryptedkey) {

    }

    @Override
    public int getDiskSize() throws RemoteException {
        return 0;
    }

    @Override
    public float getScore() throws RemoteException {
        return 0;
    }

    @Override
    public void isSafe(ComputingProtocol client, boolean safe) throws RemoteException {
    }

    @Override
    public boolean isBusy() throws RemoteException {
        return false;
    }

    @Override
    public void removeJob(int id) throws RemoteException {
        
    }









}
