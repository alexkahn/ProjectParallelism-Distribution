package be.vub.alexandre;

import java.rmi.RemoteException;

public class ClientInfo implements java.lang.Comparable<ClientInfo> {

    private ComputingProtocol client;
    private float score;
    private int waitingjobs = 0;


    public ClientInfo(ComputingProtocol client, float score) {
        this.client = client;
        this.score = score;
    }

    public void addJob(){
        this.waitingjobs = waitingjobs + 1;
    }

    public void removeJob(){
        this.waitingjobs = waitingjobs- 1;
    }

    public ComputingProtocol getClient() {
        return client;
    }

    @Override
    public int compareTo(ClientInfo client) {
        try {
            int clientSize = client.getClient().getsize();
            int thisSize = this.client.getsize();
            return Float.compare((this.waitingjobs*this.score)/clientSize, (client.getWaitingjobs()*client.score)/thisSize);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
        return 0;

    }

    public int getWaitingjobs() {
        return waitingjobs;
    }

}
