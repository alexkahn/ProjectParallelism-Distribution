package be.vub.alexandre;

import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.security.NoSuchAlgorithmException;

public class Demo {

    public static void main(String[] args) throws NoSuchAlgorithmException, InterruptedException {

        RegistryServer registryServer = new RegistryServer();
        registryServer.RegistryServer();
        TaskServer taskServer = new TaskServer();
        ClientWorker clientWorker = new ClientWorker();
        ClientWorker clientWorker1 = new ClientWorker();

        taskServer.TaskServer();
        taskServer.set(null, true, new String("./dataset_1.json"));
        clientWorker.main(1000,1, 20);
//        clientWorker1.main(500,1, 10);

        Thread.sleep(60000);
        taskServer.start(taskServer);
    }
}
