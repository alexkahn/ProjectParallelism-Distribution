package be.vub.alexandre;

import be.vub.alexandre.data.models.Comment;
import be.vub.alexandre.data.solutions.ParallelAnalyser;
import org.apache.commons.codec.binary.Base64;

import javax.crypto.*;
import javax.crypto.spec.SecretKeySpec;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.nio.ByteBuffer;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.security.*;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;

public class ClientWorker implements ComputingProtocol {

    private int size;
    private int p;
    private int id;
    private int disksize;
    private KeyPair keyPair;
    private ArrayList<Comment> comments;
    private float result;
    private ComputingProtocol registryserver;
    private ComputingProtocol clientworker;
    private ComputingProtocol taskServer;

    private Queue<ServerInfo> serverInfos= new LinkedList<ServerInfo>();

    private boolean busy = false;
    private int waitingjobs = 0;
    private int currentjob = 0;

    private Security security;
    private String encryptedSymmetricKey;

    private float benchScore;

    private String plaintext = "Distributed Reddit Analyser";

    private ParallelAnalyser analyser = new ParallelAnalyser();



    public void main(int size, int processors, int disksize) throws NoSuchAlgorithmException {
        security = new Security();
        security.generateKeyPair();
        this.size = size;
        this.p = processors;
        this.disksize = disksize;
        try {
            connect();
        } catch (RemoteException e) {
            e.printStackTrace();
        } catch (NotBoundException e) {
            e.printStackTrace();
        }

    }

    @Override
    public int getsize() throws RemoteException {
        return size;
    }

    public boolean isBusy() {
        return busy;
    }

    @Override
    public void removeJob(int id) throws RemoteException {

    }

    @Override
    public boolean store(byte[] data, PublicKey publicKey, byte[] signature, ComputingProtocol server, int listSize) throws RemoteException {
        try {
            security.verify(plaintext, signature, publicKey);
            byte[] decryptedSymmetricKey = decryptAsymmetric(encryptedSymmetricKey, server);

            if (!busy) {
                ArrayList<Comment> datalist = decryptSymmetric(data, decryptedSymmetricKey);
                comments = datalist;
            } else {
                waitingjobs = waitingjobs + 1;
                disksize = disksize - listSize;
                storeToDisk(data, waitingjobs);
                return false;
            }
            return true;
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        } catch (BadPaddingException e) {
            e.printStackTrace();
        } catch (IllegalBlockSizeException e) {
            e.printStackTrace();
        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public void connect() throws RemoteException, NotBoundException {
        try {
            Registry reg = LocateRegistry.getRegistry();
            clientworker = (ComputingProtocol) UnicastRemoteObject.exportObject(this, 0);
            registryserver = (ComputingProtocol) reg.lookup("RegistryServer");

        } catch (RemoteException e) {
            System.out.println("Client remote exception: " + e.getMessage());
        } catch (NotBoundException e) {
            System.out.println("Client not bound exception: " + e.getMessage());
        }

        id = registryserver.add(clientworker);
    }

    @Override
    public void start(ComputingProtocol taskServer){
        if (!busy) {
            busy = true;
            this.taskServer = taskServer;
            new Thread(runStart).start();
        }

    }


    Runnable runStart = new Runnable() {

        @Override
        public void run() {
            try {
                long begintime = System.currentTimeMillis();
                result = analyser.analyse(comments, p);
                long endtime = System.currentTimeMillis();
                benchScore = endtime - begintime;
                ServerInfo thisServer = serverInfos.poll();
                byte[] decryptedSymmetricKey = thisServer.getDecryptedSymmetricKey();

                byte[] encryptedResult = encryptScore(result * comments.size(), decryptedSymmetricKey);
                PrivateKey privateKey = security.getPrivateKey();
                PublicKey publicKey = security.getPublicKey();
                byte[] signature = security.sign(plaintext, privateKey);
                System.out.println(decryptedSymmetricKey);
                thisServer.getServer().getResult(encryptedResult, clientworker, publicKey, signature);  // Otherwise we will get a wrong avg size
                registryserver.removeJob(id);

                busy = false;
                if (currentjob < waitingjobs) {
                    currentjob = currentjob + 1;
                    byte[] dataFromFile = loadFromDisk(currentjob);
                    ServerInfo nextServer = serverInfos.peek();
                    decryptedSymmetricKey = nextServer.getDecryptedSymmetricKey();
                    comments = decryptSymmetric(dataFromFile, decryptedSymmetricKey);
                    disksize = disksize + comments.size();
                    start(nextServer.getServer());
                }
                else {
                    System.out.println("Job done");
                    registryserver.set(clientworker, false, null);
                }
            } catch (NoSuchPaddingException e) {
                e.printStackTrace();
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
            } catch (InvalidKeyException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (BadPaddingException e) {
                e.printStackTrace();
            } catch (IllegalBlockSizeException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };


    @Override
    public int add(ComputingProtocol clientWorker) throws RemoteException {
        return 0;
    }

    @Override
    public boolean remove(ClientWorker clientWorker) throws RemoteException {
        return false;
    }

    @Override
    public ClientWorker get() throws RemoteException {
        return null;
    }

    @Override
    public boolean set(ComputingProtocol clientWorker, boolean status, String fileAddress) throws RemoteException {
        return false;
    }

    @Override
    public void getResult(byte[] result, ComputingProtocol clientworker, PublicKey publicKey, byte[] signature) throws RemoteException {
    }

    public int getID() {
        return id;
    }

    @Override
    public PublicKey getPublicKey() throws RemoteException {
        return security.getPublicKey();
    }

    @Override
    public void sendRegisteredKey(String encryptedkey) throws RemoteException {
        encryptedSymmetricKey = encryptedkey;
    }


    // Encryption

//    //Generate Public key
//    private void generateKeyPair() throws NoSuchAlgorithmException {
//    KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance("RSA");
//    keyPairGenerator.initialize(1024);
//    KeyPair keyPair = keyPairGenerator.generateKeyPair();
//    privateKey = keyPair.getPrivate();
//    publicKey = keyPair.getPublic();
//    }

    private byte[] decryptAsymmetric(String encryptedkey, ComputingProtocol taskServer) throws InvalidKeyException, BadPaddingException, IllegalBlockSizeException, NoSuchPaddingException, NoSuchAlgorithmException {
        Cipher dipher = Cipher.getInstance("RSA");
        PrivateKey privateKey = security.getPrivateKey();
        dipher.init(Cipher.DECRYPT_MODE, privateKey);
        byte[] decryptedSymmetricKey;
        decryptedSymmetricKey = dipher.doFinal(Base64.decodeBase64(encryptedkey));

        ServerInfo serverInfo = new ServerInfo(taskServer, decryptedSymmetricKey);
        serverInfos.add(serverInfo);

        return decryptedSymmetricKey;
    }

    private ArrayList<Comment> decryptSymmetric(byte[] dataList, byte[] key) throws IOException, ClassNotFoundException, BadPaddingException, IllegalBlockSizeException, InvalidKeyException, NoSuchPaddingException, NoSuchAlgorithmException {

        Cipher cipher = Cipher.getInstance("AES");
        SecretKey secKey = new SecretKeySpec(key, "AES");

        cipher.init(Cipher.DECRYPT_MODE, secKey);
        byte[] textDecrypted = cipher.doFinal(dataList);
        ByteArrayInputStream bis = new ByteArrayInputStream(textDecrypted);
        ObjectInputStream ois = new ObjectInputStream(bis);
        ArrayList<Comment> result = (ArrayList<Comment>) ois.readObject();

        return result;

    }

    private byte[] encryptScore(float score, byte[] key) throws NoSuchPaddingException, NoSuchAlgorithmException, InvalidKeyException, IOException, BadPaddingException, IllegalBlockSizeException {
        SecretKey secKey = new SecretKeySpec(key,"AES");

        Cipher cipher = Cipher.getInstance("AES");

        cipher.init(Cipher.ENCRYPT_MODE, secKey);
        
        byte[] msg= ByteBuffer.allocate(4).putFloat(score).array();
        return cipher.doFinal(msg);
    }

    // Code to be able to save and load temporary datastore

    public int getDiskSize(){
        return disksize;
    }

    @Override
    public float getScore() throws RemoteException {
        return benchScore;
    }

    @Override
    public void isSafe(ComputingProtocol client, boolean safe) throws RemoteException {
    }

    public void storeToDisk(byte[] tempdata, int tasknumber){
        // Save the encrypted data, this makes tampering with the data more difficult
        String targetfile = "tempfile-"+ id + "-" + tasknumber; // ID shouldn't be nessecary, if one client runs only on one machine
        try {
            Files.write(Paths.get(targetfile), tempdata);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public byte[] loadFromDisk(int tasknumber) throws IOException {
        String targetfile = "tempfile-"+ id + "-" + tasknumber;
        byte[] data = Files.readAllBytes(Paths.get(targetfile));
        System.out.println("deleted: " + targetfile);
        Files.deleteIfExists(Paths.get(targetfile));
        return data;
    }


}
